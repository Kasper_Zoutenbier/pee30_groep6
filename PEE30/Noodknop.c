#include "PEE30.h"

void *KnopIngedrukt(uint_least8_t index)
{
    /* Toggle an LED */
}


void *NoodKnop()
{
    /* Call driver init functions */
    GPIO_init();

    /* Configure the LED and button pins */
    GPIO_setConfig(CONFIG_GPIO_Noodknop, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);



    /* Install Button callback */
    GPIO_setCallback(CONFIG_GPIO_Noodknop, KnopIngedrukt);

    /* Enable interrupts */
    GPIO_enableInt(CONFIG_GPIO_Noodknop);


    return (NULL);
}
