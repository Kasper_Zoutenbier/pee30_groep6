/*
 * Vanuit deze file worden alle individuele functies als pthreads aangeroepen.
 *
 */

#include "PEE30.h"


static Display_Handle display;


void *mainThread(void *arg0)
{
    pthread_t           thread;
    Status              status;
    Power               power;
    Start               start;
    Maximale            waardes;
    int                 Dutycycle = 0;


    status.Gereed = false;
    status.Noodstop = false;
    status.Overload = false;
    status.Overvoltage = false;
    status.AandrijvingGereed = false;
    status.WachtenInitialisatie = false;

    power.SpanningVoor = 0;
    power.SpanningAchter = 0;
    power.StroomAchter = 0;
    power.StroomVoor = 0;
    power.Rendement = 0;
    power.Vermogen = 0;
    power.Snelheid = 0;

    start.Initialisatie = false;
    start.startaandrijving = false;

    waardes.MaxSnelheid = 0;
    waardes.MaxSpanning = 0;
    waardes.MaxSnelheid = 0;

    Display_init();
    /* Open the UART display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL)
    {
        while (1)
            ;
    }

    //Noodknop als eerste cre�ren wegens veiligheidsredenen
    pthread_create(&thread, NULL, NoodKnop(), NULL);

    //voor comminucatie I2C
    pthread_create(&thread, NULL, I2C(&status, &start, &waardes, &power), NULL);

    //UART


    while(start.startaandrijving == true)
    {

        //Het ophalen van de metingen van stroom en spanning uit Metingen.c
        pthread_create(&thread, NULL, StroomVoor(&power), NULL);
        pthread_create(&thread, NULL, StroomAchter(&power), NULL);
        pthread_create(&thread, NULL, SpanningAchter(&power), NULL);


        //PWM met parameter Dutycycle
        pthread_create(&thread, NULL, PWM(Dutycycle), NULL);
        //pthread_create(&thread, NULL, MQTT(NULL), NULL);


        //het vermogen en het rendement in % bereken
        power.StroomVoor = 15 * Dutycycle;
        power.Vermogen = power.SpanningAchter * power.StroomAchter;
        power.Rendement = (power.Vermogen / (waardes.MaxSpanning * waardes.MaxStroom)) * 100 ;


    }

    return 0;

}

