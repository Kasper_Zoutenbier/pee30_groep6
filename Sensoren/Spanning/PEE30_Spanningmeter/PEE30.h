/*
 * PEE30.h
 *
 * Hierin komen alle includes en functies nodig voor de PEE30 code
 *
 *  Created on: 14 dec. 2021
 *      Author: Kasper Zoutenbier
 */
#ifndef PEE30_H_
#define PEE30_H_

//standaard includes
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>

/*drivers*/
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>

#include "ti_drivers_config.h"

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/ADC.h>
#include <ti/display/Display.h>


#endif /* PEE30_H_ */
