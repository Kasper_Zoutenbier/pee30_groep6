/*
 * Vanuit hier wordt alles uitgevoerd.
 */

#include "PEE30.h"


/*  functie die wordt aangeroepen na error of noodstop  */
void Ruststand();

void *mainThread(void *arg0)
{
    pthread_t           thread1,thread2;

    /*  Voor het goed zetten van alle variabelea  */
    InitVariabele();

    /*  Noodknop als eerste cre�ren wegens veiligheidsredenen  */
    pthread_create(&thread1, NULL, NoodKnop, &instellingen);

    /*  Voor comminucatie I2C  */
    pthread_create(&thread1, NULL, I2C, &instellingen);

    /*  Door geven van status van de aandrijving aan de gebruiker  */
    //pthread_create(&thread1, NULL, MQTT, &instellingen);

    /*  UART comminucatie */
    //pthread_create(&thread, NULL, UART, &instellingen);

    pthread_join(thread1,NULL);

    while(instellingen.start.startaandrijving == TRUE)
    {
        if(instellingen.status.Noodstop == TRUE)
        {
            Ruststand();
        }

        /*  Het ophalen van de metingen van stroom en spanning  */
        pthread_create(&thread2, NULL, StroomVoor, &instellingen);
        pthread_create(&thread2, NULL, StroomAchter, &instellingen);
        pthread_create(&thread2, NULL, SpanningAchter, &instellingen);

        /*  Het ophalen van de snelheid van de motor  */
        pthread_create(&thread2, NULL, SnelheidMeten, &instellingen);

        /*  PWM met parameter Dutycycle  */
        if(instellingen.PWM_Aangepast)
        {
            pthread_create(&thread2, NULL, PWM, &instellingen);
        }
        instellingen.Dutycycle = 100;

        /*  Voor berekeningen die met de stroom, spanning en snelheid te maken hebben  */
        pthread_create(&thread2, NULL, BerekenPower, &instellingen);


        pthread_join(thread2, NULL);
    }

    return 0;

}

void Ruststand()
{
    /* zolang snelheid niet 0 blijft de motor geen vermogen krijgen, oftewel wachten tot motor stil staat  */
    while(instellingen.power.Snelheid > 0)
    {
        instellingen.Dutycycle = 0;
    }
    return 0;

}
