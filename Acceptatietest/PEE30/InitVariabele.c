/*
 * InitVariabele.c
 *
 *  Created on: 19 dec. 2021
 *      Author: kaspe
 */

#include "PEE30.h"

void InitVariabele()
{
    instellingen.Dutycycle = 0;

    instellingen.maximale.MaxSnelheid = 0;
    instellingen.maximale.MaxSpanning = 0;
    instellingen.maximale.MaxStroom   = 0;

    instellingen.power.Rendement = 0;
    instellingen.power.Snelheid = 0;
    instellingen.power.SpanningAchter = 0;
    instellingen.power.SpanningVoor = 0;
    instellingen.power.StroomAchter = 0;
    instellingen.power.StroomVoor = 0;
    instellingen.power.Vermogen = 0;

    instellingen.start.Initialisatie = false;
    instellingen.start.startaandrijving = false;

    instellingen.status.AandrijvingGereed = false;
    instellingen.status.Gereed = false;
    instellingen.status.Noodstop = false;
    instellingen.status.Overload = false;
    instellingen.status.Overvoltage = false;
    instellingen.status.WachtenInitialisatie = true;
    instellingen.PWM_Aangepast = 0;
}


