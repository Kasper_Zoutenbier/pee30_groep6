#include "PEE30.h"

void *BerekenPower(Instellingen *instellingen)
{
    //het vermogen en het rendement in % bereken
    instellingen->power.SpanningVoor = (instellingen->Dutycycle / 100.0) * 15000; // dutycycle is in %
    instellingen->power.Vermogen = instellingen->power.SpanningAchter * instellingen->power.StroomAchter;
    instellingen->power.Rendement = ( instellingen->power.Vermogen * 100.0 / (instellingen->maximale.MaxSpanning * instellingen->maximale.MaxStroom)) ;
    return(NULL);
}



