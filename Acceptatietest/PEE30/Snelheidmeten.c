#include "PEE30.h"

typedef struct{
    int RPM;
    int teller;
    int seconden;
}Snelheid;

Snelheid snelheid;

void Seconden(Timer_Handle myHandle, int_fast16_t status);

void PinInterrupt(uint_least8_t index)
{
    snelheid.teller++;

}

void *SnelheidMeten(Instellingen *instellingen)
{
    Timer_Handle timer0;
    Timer_Params params;

    snelheid.RPM = 0;
    snelheid.seconden = 0;
    snelheid.teller = 0;

    /* Call driver init functions */
    GPIO_init();
    Timer_init();

    /* Enable interrupts */
    GPIO_enableInt(CONFIG_GPIO_BUTTON_0);

    Timer_Params_init(&params);
    params.period = 200000;    //200 ms
    params.periodUnits = Timer_PERIOD_US;
    params.timerMode = Timer_CONTINUOUS_CALLBACK;
    params.timerCallback = Seconden;

    timer0 = Timer_open(CONFIG_TIMER_0, &params);

    if (timer0 == NULL) {
        /* Failed to initialized timer */
        while (1) {}
    }

    if (Timer_start(timer0) == Timer_STATUS_ERROR) {
        /* Failed to start timer */
        while (1) {}
    }
    while(1)
    {
        instellingen->power.Snelheid = snelheid.RPM;
        usleep(20);
    }

    return (NULL);
}

/*
 * This callback is called every 1,000,000 microseconds, or 1 second.
 */
void Seconden(Timer_Handle myHandle, int_fast16_t status)
{
    //tijd dat een gehele seconde voorbij is
    if(snelheid.seconden > 4)
    {
        //er is een volledige minuut voorbij
        snelheid.RPM = (snelheid.teller/2);
        snelheid.teller = 0;
        snelheid.seconden = 0;
    }
    else{
        snelheid.seconden++;
    }
}
