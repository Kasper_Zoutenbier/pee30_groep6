/*
 * Copyright (c) 2018-2020, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== i2ctmp.c ========
 */
#include "PEE30.h"
#define TASKSTACKSIZE       640

#define Register            0
#define Data                1

#define slave_address       0x10


/*
 *  ======== mainThread ========
 *  voor i2c
 */
void *I2C(Instellingen *instellingen)
{
    uint16_t        sample;
    int16_t         temperature;
    uint16_t        txBuffer[2];
    uint16_t        rxBuffer[1] = {0};
    int8_t          i;
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction schrijven;



    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open(CONFIG_I2C_0, &i2cParams);
    if (i2c == NULL) {
        while (1);
    }


    /* Schrijven naar de slave */
    schrijven.slaveAddress = slave_address;
    schrijven.writeBuf   = txBuffer;
    schrijven.writeCount = 4;
    schrijven.readBuf    = rxBuffer;
    schrijven.readCount  = 0;

    /* Lezen van de slave */
    I2C_Transaction lezen;
    lezen.slaveAddress = slave_address;
    lezen.writeBuf   = txBuffer;
    lezen.writeCount = 2;
    lezen.readBuf    = rxBuffer;
    lezen.readCount  = 2;

/*naar welk register geschreven wordt*/

    txBuffer[Register]= StatusAandrijving;

/*de te schrijven data naar register 0*/

    txBuffer[1] = 0;

    if(instellingen->status.Overload)
    {
        txBuffer[Data] = OverLoad;
    }
    if(instellingen->status.Overvoltage)
    {
        txBuffer[Data] = OverVoltage;
    }
    if(instellingen->status.Noodstop)
    {
        txBuffer[Data] = NoodstopStatus;
    }
    if(instellingen->status.Gereed)
    {
        txBuffer[Data] = GereedStatus;
    }
    if(instellingen->status.WachtenInitialisatie)
    {
        txBuffer[Data] = InitialisatieStatus;
    }

    I2C_transfer(i2c, &schrijven);

/*de te schrijven data naar register 1*/

    txBuffer[Register]= RendementAandrijving;
    //de te schrijven data naar register
    if (instellingen->power.Rendement)
    {
        txBuffer[Data] = instellingen->power.Rendement;
    }
    else
    {
        txBuffer[Data] = 0;
    }

    I2C_transfer(i2c, &schrijven);

/*de te schrijven data naar register 2*/

    txBuffer[Register]= StroomAandrijving;
    //de te schrijven data naar register
    if (instellingen->power.StroomVoor)
    {
        txBuffer[Data] = instellingen->power.StroomVoor;
    }
    else
    {
        txBuffer[Data] = 0;
    }

    I2C_transfer(i2c, &schrijven);

/*de te schrijven data naar register 3*/

    txBuffer[Register] = SpanningAandrijving;
    //de te schrijven data naar register
    if (instellingen->power.SpanningVoor)
    {
        txBuffer[Data] = instellingen->power.SpanningVoor;
    }
    else
    {
        txBuffer[Data] = 0;
    }

    I2C_transfer(i2c, &schrijven);

/*de te schrijven data naar register 4*/

    txBuffer[Register] = SnelheidAandrijving;
    //de te schrijven data naar register
    if (instellingen->power.Snelheid)
    {
        txBuffer[Data] = instellingen->power.Snelheid;
    }
    else
    {
        txBuffer[Data] = 0;
    }
    I2C_transfer(i2c, &schrijven);


/*welk register je wilt uitlezen*/

/*Het start register uit lezen, Vanaf hier alleen nog read only van de MCU*/
    txBuffer[Register] = StartRegister;

    I2C_transfer(i2c, &lezen);

    if(rxBuffer[0] > 0)
    {
        int waarde =  rxBuffer[0];
        switch(waarde)
        {
        case StartAandrijving:
            instellingen->start.startaandrijving = true;
            break;

        case StartInitialisatie:
            instellingen->start.Initialisatie = true;
            break;
        }
    }
/*de instellingen van de maximale waardes uitlezen*/

    txBuffer[Register] = MaximaleStroomInstellen;
    I2C_transfer(i2c, &lezen);
    if(rxBuffer[0])
    {
        instellingen->maximale.MaxStroom = rxBuffer[0];
    }

    txBuffer[Register] = MaximaleSpanningInstellen;
    I2C_transfer(i2c, &lezen);
    if(rxBuffer[0])
    {
        instellingen->maximale.MaxSpanning = rxBuffer[0];
    }

    txBuffer[Register] = MaximaleSnelheidInstellen;
    I2C_transfer(i2c, &lezen);
    if(rxBuffer[0])
    {
        instellingen->maximale.MaxSnelheid = rxBuffer[0];
    }

    I2C_close(i2c);
    instellingen->start.startaandrijving = true;

    return (NULL);
}
