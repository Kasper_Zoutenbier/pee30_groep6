/*
 * PEE30.h
 *
 * Hierin komen alle includes en functies nodig voor de PEE30 code
 *
 *  Created on: 14 dec. 2021
 *      Author: Kasper Zoutenbier
 */
#ifndef PEE30_H_
#define PEE30_H_

#define THREADSTACKSIZE 12000

//standaard includes
#include <stdio.h>
#include <stddef.h>
#include <unistd.h>

/* POSIX Header files */
#include <pthread.h>

/* RTOS header files */
#include <ti/sysbios/BIOS.h>
#include <ti/drivers/Board.h>


/*drivers*/
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/PWM.h>
#include <ti/drivers/Timer.h>
#include <ti/display/Display.h>

#include "ti_drivers_config.h"


typedef struct
{
    _Bool Overload;
    _Bool Overvoltage;
    _Bool Noodstop;
    _Bool Gereed;
    _Bool AandrijvingGereed;
    _Bool WachtenInitialisatie;
}Status;

typedef struct
{
    int StroomVoor;
    int StroomAchter;
    int SpanningVoor;
    int SpanningAchter;
    int Rendement;
    int Vermogen;
    int Snelheid;
}Power;

typedef struct
{
    _Bool Initialisatie;
    _Bool startaandrijving;

}Start;

typedef struct
{
    int MaxSpanning;
    int MaxStroom;
    int MaxSnelheid;
}Maximale;


typedef struct
{
    Status status;
    Power  power;
    Start  start;
    Maximale maximale;
    int Dutycycle;
    int PWM_Aangepast;
}Instellingen;

Instellingen instellingen;


enum RegisterNummer
{
    StatusAandrijving = 0,
    RendementAandrijving,
    StroomAandrijving,
    SpanningAandrijving,
    SnelheidAandrijving,
    StartRegister,
    MaximaleStroomInstellen,
    MaximaleSpanningInstellen,
    MaximaleSnelheidInstellen
};


enum StatusToestand
{
    OverLoad = 0,
    OverVoltage = 1,
    NoodstopStatus = 3,
    GereedStatus = 7,
    InitialisatieStatus = 31
};

enum StartToestand
{
    StartAandrijving = 1,
    StartInitialisatie = 2
};


/*linken van files*/
void InitVariabele();
void *StroomVoor(Instellingen *instellingen);
void *StroomAchter(Instellingen *instellingen);
void *SpanningAchter(Instellingen *instellingen);
void *SnelheidMeten(Instellingen *instellingen);
void MQTT(Instellingen * instellingen);
void *I2C(Instellingen* instellingen);
void *PWM(Instellingen* instellingen);
void *NoodKnop(Instellingen* instellingen);
void *BerekenPower(Instellingen *instellingen);







#endif /* PEE30_H_ */
