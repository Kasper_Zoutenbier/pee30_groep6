#include "PEE30.h"

typedef struct{
    int RPM;
    int teller;
    int seconden;
}Snelheid;

Snelheid snelheid;

int timer = 0;

Timer_Handle timer1;
Timer_Params params;

void Seconden(Timer_Handle myHandle, int_fast16_t status);

void PinInterrupt(uint_least8_t index)
{
    snelheid.teller++;

}

void *SnelheidMeten(Instellingen *instellingen)
{
    if(!(timer))
    {
        snelheid.RPM = 0;
        snelheid.seconden = 0;
        snelheid.teller = 0;

        Timer_Params_init(&params);
        params.period = 1000000;    //1 s
        params.periodUnits = Timer_PERIOD_US;
        params.timerMode = Timer_ONESHOT_CALLBACK;
        params.timerCallback = Seconden;

        /* Enable interrupts */
        GPIO_enableInt(CONFIG_GPIO_BUTTON_0);

        Timer_init();
        timer1 = Timer_open(CONFIG_TIMER_1, &params);
    }
    timer = 1;

    if (timer1 == NULL)
    {
        /* Failed to initialized timer */
        while (1)  {}
    }

    if (Timer_start(timer1) == Timer_STATUS_ERROR)
    {
        /* Failed to start timer */
        while (1) {}
    }
    while(1)
    {
        instellingen->power.Snelheid = snelheid.RPM;
    }

    return (NULL);
}

/*
 * This callback is called every 1,000,000 microseconds, or 1 second.
 */
void Seconden(Timer_Handle myHandle, int_fast16_t status)
{
    snelheid.RPM = (snelheid.teller / 2);
    snelheid.teller = 0;
    snelheid.seconden = 0;
    usleep(1);
}
