#include "PEE30.h"

/* ADC sample count */
#define ADC_SAMPLE_COUNT  (10)

#define THREADSTACKSIZE   (768)

/* ADC conversion result variables */
uint16_t adcValue0;
uint32_t adcValue0MicroVolt;
uint16_t adcValue1[ADC_SAMPLE_COUNT];
uint32_t adcValue1MicroVolt[ADC_SAMPLE_COUNT];

static Display_Handle display;




void *StroomVoor(Instellingen *instellingen)
{
    uint16_t     i;
    ADC_Handle   adc;
    ADC_Params   params;
    int_fast16_t res;
    double Noemer=0;

    /* Call driver init functions */
    ADC_init();

    ADC_Params_init(&params);

    while(1)
    {
        adc = ADC_open(CONFIG_ADC_0, &params);
        double Noemer=0;


        if (adc == NULL) {
            //Display_printf(display, 0, 0, "Error initializing CONFIG_ADC_1\n");
            while (1);
        }

        for (i = 0; i < ADC_SAMPLE_COUNT; i++) {
            res = ADC_convert(adc, &adcValue1[i]);

            if (res == ADC_STATUS_SUCCESS) {

                Noemer += (adcValue1[i] *845.8768114); // adcValue[i]* 845.894067
            }
            else {
                //Display_printf(display, 0, 0, "Stroomvoor meting  failed (%d)\n", i);
                while(1);
            }


        }

        double Gemmidelde = Noemer / ADC_SAMPLE_COUNT;
        double InMiliVolt = Gemmidelde / 1000;
        double InmiliAmpere = (InMiliVolt - 1000)  * 10;     //in dit geval er van uit gaande dat spanning 1V is bij 0 A

        instellingen->power.StroomVoor = InmiliAmpere;


        ADC_close(adc);
        usleep(1000);
    }

    return (NULL);
}

void *StroomAchter(Instellingen *instellingen)
{
    uint16_t     i;
    ADC_Handle   adc;
    ADC_Params   params;
    int_fast16_t res;
    double Noemer=0;
    /* Call driver init functions */
    ADC_init();


    ADC_Params_init(&params);

    while(1)
    {
        adc = ADC_open(CONFIG_ADC_0, &params);

        if (adc == NULL) {
            while (1);
        }

        for (i = 0; i < ADC_SAMPLE_COUNT; i++) {
            res = ADC_convert(adc, &adcValue1[i]);

            if (res == ADC_STATUS_SUCCESS) {

                Noemer += (adcValue1[i] *845.8768114); // adcValue[i]* 845.894067
            }
            else {

               // Display_printf(display, 0, 0, "StroomAchter meting failed (%d)\n", i);
                while(1);
            }

        }

        double Gemmidelde = Noemer / ADC_SAMPLE_COUNT;
        double InMiliVolt = Gemmidelde / 1000;
        double InmiliAmpere = (InMiliVolt - 1000)  * 10;     //in dit geval er van uit gaande dat spanning 1V is bij 0 A

        instellingen->power.StroomAchter = InmiliAmpere;


        ADC_close(adc);
        usleep(1000);
    }

    return (NULL);
}

void *SpanningAchter(Instellingen *instellingen)
{
    uint16_t     i;
    ADC_Handle   adc;
    ADC_Params   params;
    int_fast16_t res;
    double Noemer=0;
    /* Call driver init functions */
    ADC_init();


    ADC_Params_init(&params);

    while(1)
    {
        adc = ADC_open(CONFIG_ADC_0, &params);


        if (adc == NULL) {
            //Display_printf(display, 0, 0, "Error initializing CONFIG_ADC_1\n");
            while (1);
        }

        for (i = 0; i < ADC_SAMPLE_COUNT; i++) {
            res = ADC_convert(adc, &adcValue1[i]);

            if (res == ADC_STATUS_SUCCESS) {

                Noemer += (adcValue1[i] *845.8768114); // adcValue[i]* 845.894067
            }
            else {
                while(1);
                //Display_printf(display, 0, 0, "Spanningsmeting achter failed (%d)\n", i);

            }



        }

        double Gemmidelde = Noemer / ADC_SAMPLE_COUNT;
        double InMiliVolt = Gemmidelde / 1000;

        instellingen->power.SpanningAchter = InMiliVolt;

        ADC_close(adc);
        usleep(1000);
    }

    return (NULL);
}


