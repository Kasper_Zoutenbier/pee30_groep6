#include "PEE30.h"

enum {
    Ingedrukt=0, NietIngedrukt
};


int knop= NietIngedrukt;

void KnopIngedrukt(uint_least8_t index)
{
    knop = GPIO_read(CONFIG_GPIO_BUTTON_0);
}




void *NoodKnop(Instellingen *instellingen)
{
    /* Call driver init functions */
    GPIO_init();

    /* Enable interrupts */
    GPIO_enableInt(CONFIG_GPIO_BUTTON_0);
    while(1)
    {
        switch(knop)
        {
        case Ingedrukt:
            instellingen->status.Noodstop = true;
            break;
        case NietIngedrukt:
            instellingen->status.Noodstop = false;
            break;
        }
        usleep(1);
    }
    return (NULL);
}
