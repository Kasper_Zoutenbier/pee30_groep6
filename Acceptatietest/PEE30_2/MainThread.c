/*
 * Vanuit hier wordt alles uitgevoerd.
 */

#include "PEE30.h"


/*  functie die wordt aangeroepen na error of noodstop  */
void *Ruststand(void *arg0);

void *mainThread(void *arg0)
{
    pthread_t           thread1,thread2;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;

    /*  Voor het goed zetten van alle variabelea  */
    InitVariabele();

    GPIO_init();
    I2C_init();
    UART_init();
    Timer_init();

    /* Initialize the attributes structure with default values */
    pthread_attr_init(&attrs);

    /* Set priority, detach state, and stack size attributes */
    priParam.sched_priority = 1;
    retc = pthread_attr_setschedparam(&attrs, &priParam);
    retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);

    if (retc != 0)
    {
        /* failed to set attributes */
        while (1) {}

    }
    while(1)
    {
        /*  Noodknop als eerste cre�ren wegens veiligheidsredenen  */
        pthread_create(&thread1, NULL, NoodKnop, &instellingen);
        if (instellingen.status.Noodstop == TRUE)
        {
            pthread_create(&thread1, NULL, Ruststand, NULL);
        }

        /*  Voor comminucatie I2C  */
        pthread_create(&thread1, NULL, I2C, &instellingen);

        /*  Door geven van status van de aandrijving aan de gebruiker  */
        //pthread_create(&thread1, NULL, MQTT, &instellingen);

        /*  UART comminucatie */
        //pthread_create(&thread, NULL, UART, &instellingen);

        pthread_join(thread1, NULL);

        if(instellingen.start.startaandrijving == TRUE)
        {


            /*  Het ophalen van de metingen van stroom en spanning  */
            pthread_create(&thread2, NULL, StroomVoor, &instellingen);
            pthread_create(&thread2, NULL, StroomAchter, &instellingen);
            pthread_create(&thread2, NULL, SpanningAchter, &instellingen);

            /*  Het ophalen van de snelheid van de motor  */
            pthread_create(&thread2, NULL, SnelheidMeten, &instellingen);

            /*  PWM met parameter Dutycycle  */
            if(instellingen.PWM_Aangepast)
            {
                pthread_create(&thread2, NULL, PWM, &instellingen);
            }
            instellingen.Dutycycle = 100;

            /*  Voor berekeningen die met de stroom, spanning en snelheid te maken hebben  */
            pthread_create(&thread2, NULL, BerekenPower, &instellingen);


            pthread_join(thread2, NULL);
        }
    }
    return 0;

}

void *Ruststand(void * arg0)
{
    /* zolang snelheid niet 0 blijft de motor geen vermogen krijgen, oftewel wachten tot motor stil staat  */
    while(instellingen.power.Snelheid > 0)
    {
        instellingen.Dutycycle = 0;

    }
    return 0;

}
