/*
 * Copyright (c) 2018-2020, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "PEE30.h"

#define TASKSTACKSIZE       640

#define Register 0

#define slave_address 0x10

static Display_Handle display;

/*
 *  ======== mainThread ========
 *  voor i2c voor meten van stroom via adc
 *  omdat cc3220s te weinig beschikbare/te gebruiken ADC heeft
 */
void *I2C_Stroommeten(void *arg0)
{
    uint16_t        sample;
    int16_t         temperature;
    uint16_t         txBuffer[3];
    uint16_t         rxBuffer[2] = {0,0};
    int8_t          i;
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction schrijven;

    /* Call driver init functions */
    Display_init();
    GPIO_init();
    I2C_init();



    /* Open the UART display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL) {
        while (1);
    }
    Display_printf(display, 0,0, "-------------------------------------------------------------------------");
    /* Turn on user LED */
    Display_printf(display, 0, 0, "Starting the i2ctmp example\n");

    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open(CONFIG_I2C_0, &i2cParams);
    if (i2c == NULL) {
        Display_printf(display, 0, 0, "Error Initializing I2C\n");
        while (1);
    }
    else {
        Display_printf(display, 0, 0, "I2C Initialized!\n");
    }

    /* Schrijven naar de slave */
    schrijven.slaveAddress = slave_address;
    schrijven.writeBuf   = txBuffer;
    schrijven.writeCount = 2;
    schrijven.readBuf    = rxBuffer;
    schrijven.readCount  = 0;

    /* Lezen van de slave */
    I2C_Transaction lezen;
    lezen.slaveAddress = slave_address;
    lezen.writeBuf   = txBuffer;
    lezen.writeCount = 1;
    lezen.readBuf    = rxBuffer;
    lezen.readCount  = 2;

    //naar welk register geschreven wordt
    txBuffer[Register]=0x01;

    //de te schrijven data naar register
    txBuffer[1] = 5000;
    txBuffer[2] = 1;

    Display_printf(display, 0, 0, "Schrijven van txBuffer[1]: %d \n", txBuffer[1]);
    Display_printf(display, 0, 0, "Schrijven van txBuffer[2]: %d \n", txBuffer[2]);

    I2C_transfer(i2c, &schrijven);

    //welk register je wilt uitlezen
    txBuffer[Register]= 0x01;

    I2C_transfer(i2c, &lezen);

    Display_printf(display, 0, 0, "lezen van rxBuffer[0]: %d \n", rxBuffer[0]);
    Display_printf(display, 0, 0, "lezen van rxBuffer[1]: %d \n", rxBuffer[1]);

   // while(1)
    {

    }


    I2C_close(i2c);
    Display_printf(display, 0, 0, "I2C closed!");
    Display_printf(display, 0,0, "-------------------------------------------------------------------------\n");


    return (NULL);
}
