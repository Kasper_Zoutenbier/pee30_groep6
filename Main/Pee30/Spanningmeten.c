#include "PEE30.h"

/* ADC sample count */
#define ADC_SAMPLE_COUNT  (10)

#define THREADSTACKSIZE   (768)

/* ADC conversion result variables */
uint16_t adcValue0;
uint32_t adcValue0MicroVolt;
uint16_t adcValue1[ADC_SAMPLE_COUNT];
uint32_t adcValue1MicroVolt[ADC_SAMPLE_COUNT];

static Display_Handle display;



/*
 *  ======== threadFxn1 ========
 *  Open a ADC handle and get an array of sampling results after
 *  calling several conversions.
 */
void *ADCmeten(void *arg0)
{
    uint16_t     i;
    ADC_Handle   adc;
    ADC_Params   params;
    int_fast16_t res;
    double Noemer=0;

    ADC_Params_init(&params);
    adc = ADC_open(CONFIG_ADC_0, &params);

    if (adc == NULL) {
        Display_printf(display, 0, 0, "Error initializing CONFIG_ADC_1\n");
        while (1);
    }

    for (i = 0; i < ADC_SAMPLE_COUNT; i++) {
        res = ADC_convert(adc, &adcValue1[i]);

        if (res == ADC_STATUS_SUCCESS) {

            adcValue1MicroVolt[i] = ADC_convertRawToMicroVolts(adc, adcValue1[i]);

            Display_printf(display, 0, 0, "CONFIG_ADC_1 raw result (%d): %d\n", i,
                           adcValue1[i]);
            Display_printf(display, 0, 0, "CONFIG_ADC_1 convert result (%d): %d uV\n", i,
                adcValue1MicroVolt[i]);
            Noemer += (adcValue1[i] *845.8768114); // adcValue[i]* 845.894067
        }
        else {
            Display_printf(display, 0, 0, "CONFIG_ADC_1 convert failed (%d)\n", i);
        }


    }

    double Gemmidelde = Noemer / ADC_SAMPLE_COUNT;
    Display_printf(display,0,0, "Het gemmidelde is : %3.f uV\n", Gemmidelde);
    double InMiliVolt = Gemmidelde / 1000;
    Display_printf(display,0,0, "Het gemmidelde voltage is : %3.f mV\n", InMiliVolt);
    double InVolt = InMiliVolt / 1000;
    Display_printf(display,0,0, "Het gemmidelde voltage is : %3.f V\n", InVolt);
    double InmiliAmpere = (InMiliVolt - 1000)  * 10;     //in dit geval er van uit gaande dat spanning 1V is bij 0 A
    Display_printf(display,0,0, "Het gemmidelde miliAmpere is : %3.f mA\n", InmiliAmpere);
    double InAmpere = InmiliAmpere / 1000;
    Display_printf(display,0,0, "Het gemmidelde Ampere is : %3.f A\n", InAmpere);




    ADC_close(adc);

    return (NULL);
}

/*
 *  ======== mainThread ========
 *  Voor meten van de spanning
 */
void *Spanningmeten(void *arg0)
{
    pthread_t           thread0, thread1;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    /* Call driver init functions */
    ADC_init();
    Display_init();

    /* Open the display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL) {
        /* Failed to open display driver */
        while (1);
    }

    Display_printf(display, 0, 0, "Starting the acdsinglechannel example\n");

    /* Create application threads */
    pthread_attr_init(&attrs);

    detachState = PTHREAD_CREATE_DETACHED;
    /* Set priority and stack size attributes */
    retc = pthread_attr_setdetachstate(&attrs, detachState);
    if (retc != 0) {
        /* pthread_attr_setdetachstate() failed */
        while (1);
    }

    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0) {
        /* pthread_attr_setstacksize() failed */
        while (1);
    }

    /* Create threadFxn1 thread */
    retc = pthread_create(&thread1, &attrs, ADCmeten, (void* )0);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }

    return (NULL);
}
