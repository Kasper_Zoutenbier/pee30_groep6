/*
 * Vanuit deze file worden alle individuele functies als pthreads aangeroepen.
 *
 */

#include "PEE30.h"


static Display_Handle display;


void *mainThread(void *arg0)
{
    pthread_t           thread1,thread2;

    InitVariabele();

    Display_init();
    /* Open the UART display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL)
    {
        while (1)
            ;
    }

    //Noodknop als eerste cre�ren wegens veiligheidsredenen
    pthread_create(&thread1, NULL, NoodKnop, &instellingen);

    //voor comminucatie I2C
    pthread_create(&thread1, NULL, I2C, &instellingen);

    //UART
    pthread_join(thread1,NULL);

    while(instellingen.start.startaandrijving == true)
    {

        //Het ophalen van de metingen van stroom en spanning uit Metingen.c
        pthread_create(&thread2, NULL, StroomVoor, &instellingen);
        pthread_create(&thread2, NULL, StroomAchter, &instellingen);
        pthread_create(&thread2, NULL, SpanningAchter, &instellingen);


        //PWM met parameter Dutycycle
        pthread_create(&thread2, NULL, PWM, &instellingen);
        //pthread_create(&thread, NULL, MQTT(NULL), NULL);

        pthread_join(thread2, NULL);

        //het vermogen en het rendement in % bereken
        instellingen.power.SpanningVoor = 15 * instellingen.Dutycycle;
        instellingen.power.Vermogen = instellingen.power.SpanningAchter * instellingen.power.StroomAchter;
        instellingen.power.Rendement = ( instellingen.power.Vermogen / (instellingen.maximale.MaxSpanning * instellingen.maximale.MaxStroom)) * 100 ;

    }

    return 0;

}

