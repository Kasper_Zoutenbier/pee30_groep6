#include "PEE30.h"

int knop;
void *KnopIngedrukt(uint_least8_t index)
{
    if (index == 0)
    {
        knop = 0;
    }else{
        knop = 1;
    }
}

void *NoodKnop(Instellingen* instellingen)
{
    /* Call driver init functions */
    GPIO_init();

    /* Configure the LED and button pins */
    GPIO_setConfig(CONFIG_GPIO_Noodknop, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);



    /* Install Button callback */
    GPIO_setCallback(CONFIG_GPIO_Noodknop, KnopIngedrukt);


    /* Enable interrupts */
    GPIO_enableInt(CONFIG_GPIO_Noodknop);

    while(1)
    {
        if(knop == 1)
        {
            instellingen->status.Noodstop = true;
        }
        else if(knop == 0)
        {
            instellingen->status.Noodstop = false;
        }
        usleep(100);
    }


    return (NULL);
}
